import Vue from 'vue'
import Vuex from 'vuex'
import { api, prices } from '@/api'
import { DateTime } from 'luxon'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    token: null,
    user: null,
    prices: []
  },
  mutations: {
    setToken(state, token) {
      state.token = token
    },

    setUser(state, user) {
      state.user = user
    },

    setPrices(state, data) {
      state.prices = data
    }
  },
  actions: {
    async loadToken({ commit }) {
      const token = localStorage.getItem('token')
      if (token) {
        const user = await api('me', {
          headers: {
            Authorization: 'Bearer ' + token
          }
        }).json()
        if (user) {
          commit('setToken', token)
          commit('setUser', user)
        }
      }
    },

    setToken({ commit }, token) {
      localStorage.setItem('token', token)
      commit('setToken', token)
    },

    setUser({ commit }, user) {
      commit('setUser', user)
    },

    clearState({ commit }) {
      localStorage.removeItem('token')
      commit('setToken', '')
      commit('setUser', null)
    },

    async fetchPrices({ commit }) {
      const data = await prices.get()
      const userDate = DateTime.local()

      const pricesData = data
        .sort((a, b) => b.price - a.price)
        .filter(price => {
          const them = DateTime.local().setZone(price.user.timezone)
          const diff = userDate.offset - them.offset
          const priceStart = DateTime.fromISO(price.price_date)
            .plus({
              hours: price.morning ? 0 : 12
            })
            .plus({ minutes: diff })
          const priceEnd = DateTime.fromISO(price.price_date)
            .plus({
              hours: price.morning ? 12 : 22
            })
            .plus({ minutes: diff })
          return userDate >= priceStart && userDate < priceEnd
        })

      commit('setPrices', pricesData)
    }
  },
  modules: {}
})
