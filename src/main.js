import Vue from 'vue'
import App from './App.vue'
import VueMaterial from 'vue-material'
import 'vue-material/dist/vue-material.min.css'
import 'vue-material/dist/theme/default.css'
import store from './store'
import { mapActions } from 'vuex'
import VueApexCharts from 'vue-apexcharts'

Vue.use(VueMaterial)
Vue.component('apexchart', VueApexCharts)

Vue.config.productionTip = false

new Vue({
  store,
  render: h => h(App),
  methods: {
    ...mapActions(['loadToken'])
  },
  mounted() {
    this.loadToken()
  }
}).$mount('#app')
