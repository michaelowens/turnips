import ky from 'ky'
import users from './users'
import prices from './prices'
import store from '@/store'

export const api = ky.extend({
  prefixUrl: process.env.API_URL,
  hooks: {
    beforeRequest: [
      request => {
        const token = store.state.token
        if (token) {
          request.headers.set('Authorization', 'Bearer ' + token)
        }
      }
    ]
  }
})

export { users, prices }
