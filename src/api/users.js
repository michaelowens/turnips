import { api } from './'

export default {
  get() {
    return api('user').json()
  }
}
