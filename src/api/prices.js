import { api } from './'

export default {
  get() {
    return api('price').json()
  },

  chart() {
    return api('chart').json()
  },

  add(price, price_date, morning) {
    return api
      .post('price', {
        json: { price, price_date, morning }
      })
      .json()
  }
}
