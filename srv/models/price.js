'use strict'
module.exports = (sequelize, DataTypes) => {
  const Price = sequelize.define(
    'price',
    {
      price_date: DataTypes.DATEONLY,
      morning: DataTypes.BOOLEAN,
      price: DataTypes.INTEGER
    },
    {}
  )
  Price.associate = function(models) {
    Price.belongsTo(models.user)
  }
  return Price
}
