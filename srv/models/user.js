'use strict'
module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define(
    'user',
    {
      username: DataTypes.STRING,
      timezone: DataTypes.STRING,
      access_token: DataTypes.STRING,
      refresh_token: DataTypes.STRING,
      discord_id: DataTypes.STRING
    },
    {}
  )
  User.associate = function(models) {
    User.hasMany(models.price)
  }
  return User
}
