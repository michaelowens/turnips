import express from 'express'
import cors from 'cors'
import queryString from 'query-string'
// import { sequelize, User, Price } from './db'
import models, { sequelize } from './models'
import { Sequelize, Op } from 'sequelize'
import fetchDiscordTokens, {
  DISCORD_OAUTH_BASE_URL,
  DISCORD_OAUTH_SCOPES,
  getCurrentUser
} from './discord'
import passport from 'passport'
import { Strategy as BearerStrategy } from 'passport-http-bearer'
import jwt from 'jsonwebtoken'
import { DateTime } from 'luxon'

if (process.env.NODE_ENV !== 'production') {
  require('dotenv').config()
}

export default (app, http) => {
  passport.use(
    new BearerStrategy(function(token, done) {
      jwt.verify(token, process.env.JWT_KEY, function(err, decoded) {
        if (err) return done(err)
        models.user
          .findOne({ where: { discord_id: decoded.id } })
          .then(user => {
            if (user) {
              return done(null, user)
            } else {
              return done(null, false)
            }
          })
          .catch(err => done(err, false))
      })
    })
  )

  app.use(cors())
  app.use(passport.initialize())
  app.use(express.json())

  app.get('/price', (req, res) => {
    const now = new Date()
    const timeStart = new Date(
      now.getFullYear(),
      now.getMonth(),
      now.getDate(),
      now.getHours() < 12 ? 0 : 12,
      0,
      0,
      0
    )
    const timeEnd = new Date(
      now.getFullYear(),
      now.getMonth(),
      now.getDate(),
      now.getHours() < 12 ? 11 : 23,
      59,
      0,
      0
    )

    models.price
      .findAll({
        include: [models.user],
        where: {
          price_date: {
            // TODO: Fix this as this is useless, considering we only store the date, not time.
            // But we might need to deal with northern & southern hemisphere
            [Op.gte]: timeStart,
            [Op.lte]: timeEnd
          }
        },
        order: [['price', 'DESC']]
      })
      .then(prices => res.json(prices))
  })

  app.get('/chart', (req, res) => {
    const weekStart = DateTime.local()
      .setZone('utc')
      .startOf('week')

    models.price
      .findAll({
        include: [models.user],
        where: {
          price_date: {
            // TODO: Fix this as this is useless, considering we only store the date, not time.
            // But we might need to deal with northern & southern hemisphere
            [Op.gte]: weekStart.toISODate()
          }
        },
        order: [
          [models.user, 'username', sequelize.literal('COLLATE NOCASE'), 'ASC']
        ]
      })
      .then(prices => {
        let users = {}
        prices.forEach(price => {
          if (!(price.user.username in users)) {
            users[price.user.username] = {
              name: price.user.username,
              data: Array(12).fill(null)
            }
          }
          const d = DateTime.fromISO(price.price_date, { zone: 'utc' }).set({
            hour: price.morning ? 0 : 12
          })
          const diff = d.diff(weekStart, 'hours').toObject()
          users[price.user.username].data[diff.hours / 12] = price.price
        })
        res.json(Object.values(users))
      })
  })

  app.post('/discord', async (req, res) => {
    const { code, timezone } = req.body

    try {
      const { access_token, refresh_token } = await fetchDiscordTokens(code)

      const discordUser = await getCurrentUser(access_token)

      sequelize
        .sync()
        .then(() =>
          models.user.findOrCreate({
            where: { discord_id: discordUser.id },
            defaults: {
              username: discordUser.username,
              timezone,
              access_token,
              refresh_token
            }
          })
        )
        .then(([user]) => {
          const { username, timezone, discord_id } = user
          res.json({
            user: { username, timezone },
            token: jwt.sign({ id: discord_id }, process.env.JWT_KEY)
          })
        })
    } catch (error) {
      console.log(error)
      res.status(500).json({ message: error.toString() })
    }
  })

  app.get('/discord/redirect', async (req, res) => {
    const _params = {
      response_type: 'code',
      client_id: process.env.DISCORD_CLIENT_ID,
      redirect_uri: process.env.DISCORD_CALLBACK_URL,
      scope: DISCORD_OAUTH_SCOPES.join(' '),
      state: null
    }

    const params = queryString.stringify(_params)
    res.send(`${DISCORD_OAUTH_BASE_URL}?${params}`)
  })

  const authenticate = function(req, res, next) {
    passport.authenticate('bearer', function(err, user) {
      if (err) return next(err)
      if (user) {
        req.user = user
        return next()
      } else {
        return res.status(401).json({ status: 'error', code: 'unauthorized' })
      }
    })(req, res, next)
  }

  app.get('/me', authenticate, async (req, res) => {
    const { username, timezone } = req.user
    res.json({ username, timezone })
  })

  app.post('/price', authenticate, (req, res) => {
    sequelize
      .sync()
      .then(async () => {
        const { user, body } = req
        const p = await models.price.create({
          price_date: body.price_date,
          morning: body.morning,
          price: +body.price
        })
        p.setUser(user)
        return p
      })
      .then(newPrice => res.json(newPrice))
  })
}
