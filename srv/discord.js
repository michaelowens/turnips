import got from 'got'
import queryString from 'query-string'

export const DISCORD_OAUTH_BASE_URL =
  'https://discordapp.com/api/oauth2/authorize'
export const DISCORD_OAUTH_SCOPES = ['identify'] //, 'email'] // do we need email?

export const getCurrentUser = async access_token =>
  await got('https://discordapp.com/api/v6/users/@me', {
    headers: {
      authorization: `Bearer ${access_token}`
    }
  }).json()

const sendOauthRequest = opts =>
  got
    .post('https://discordapp.com/api/oauth2/token', {
      headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
      body: queryString.stringify({
        ...opts,
        redirect_uri: process.env.DISCORD_CALLBACK_URL,
        client_id: process.env.DISCORD_CLIENT_ID,
        client_secret: process.env.DISCORD_CLIENT_SECRET,
        scope: DISCORD_OAUTH_SCOPES.join(' ')
      })
    })
    .json()

export default code =>
  new Promise((resolve, reject) =>
    sendOauthRequest({
      code,
      grant_type: 'authorization_code'
    })
      .then(resolve)
      .catch(reject)
  )
