# turnips

A web app to share your Animal Crossing turnip prices to maximize your profits together.

The repository contains both the frontend and backend code for the website. If you run the development environment it generally starts the frontend on port `8080` and the backend api on port `3000`. In production the site runs on 1 machine and we use dokku to run backend api and host the frontend app on the same address.

## Project setup

```
yarn install
```

### Compiles and hot-reloads for development

```bash
yarn serve # frontend
yarn run express # backend api
```

### Deployment

When using dokku and you have your dokku server configured as a remote you can simply push to it.

```bash
git push dokku master
```

### Migrations

Migrations run automatically after deployment and before scheduling the new dokku container. For more info on migrations take a look at the [Sequelize](https://sequelize.org/) manual.
